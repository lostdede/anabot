/*
  Firestore Config
*/

const Firestore = require('@google-cloud/firestore');

const firestore = new Firestore({
	projectId: process.env.ANADEAR,
	keyFilename: process.env.KEY_ANADEAR
});

const museagramFirestore = new Firestore({
	projectId: process.env.MUSEAGRAM,
	keyFilename: process.env.KEY_MUSEAGRAM
});

module.exports.VerifiedDocument = firestore.doc(process.env.VERIFIED);
module.exports.UnverifiedDocument = firestore.doc(process.env.NOTVERIFIED);
module.exports.suggestions = museagramFirestore.collection(process.env.SUGGESTIONS);
module.exports.users = museagramFirestore.collection(process.env.USERSTORE);
