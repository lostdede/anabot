require('dotenv').config();
const Logs = require('../Services/logger').Log;
const S3 = require('../Services/s3').S3;
const BH = require('../Services/bothandler').botHandler;

it('Logger: Active', () => {
  expect(Logs).toBeDefined();
});

it('S3: Online',() => {
  expect(S3).toBeDefined();
});

it('Bot Handler', () => {
  expect(BH).toBeDefined();
});
