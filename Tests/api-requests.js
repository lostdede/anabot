const request = require('request');
const host = process.argv[2] ? process.argv[2] : 'http://localhost/api/anabot/hooks';

const routes = {
  default:`/`,
  bitbucket: {
    anabot: `${host}/bitbucket/anabot/repo`
  },
  ifttt: {
    jemCalendar: `${host}/ifttt/google-calendar/update`,
    PortMania: {
      entry: `${host}/ifttt/port-mania/entry`
    }
  }
};

function start(){
  // Bitbucket Push
  request.post(
    `${host}/bitbucket/anabot/repo`,
    {json: {push:{changes:[{new:{target:{message:'New and improved updates'}}},{new:{target:{message:'New and improved updates a second time'}}}]},
    actor:{display_name: 'Jen Calter'},
    repository:{name:'Anabot'}}},
    function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(`Bitbucket Route: ${JSON.stringify(body)}`)
        }
    }
  );

  // Port Entry
  request.post(
    `${host}/ifttt/port-mania/entry`,
    {json: {test:'test'}},
    function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(`PortMania Route: ${JSON.stringify(body)}`)
        }
    }

  );

  // Jem Calendar
  request.post(
    `${host}/ifttt/google-calendar/update`,
    {json:{test:'test'}},
    function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(`Google Calndar Route: ${JSON.stringify(body)}`)
        }
    }
  );
};

start();
