/*

  Module to handle the parsing of user input.

*/

module.exports.Parser = {
  isCommand:isCommand,
  commandWithInputs:commandWithInputs,
  getCommandInputs:getCommandInputs,
  whichCommand:whichCommand
}

/*************** Recognizing Commands */

// isCommand recieves a string and determines if it is a command
function isCommand(str) {
    if (str[0] === '/'){
      return true;
    }
    else {
      return false;
    }
}

// commandWithInputs checks if the issued command has paramenters. Return command and params
function commandWithInputs(str) {
  let words = str.split(' ');
  if (words.length > 1){
    return true;
  }
  else {
    return false;
  }
}

// getCommandInputs retrieves the inputs from a given command
function getCommandInputs(str){
  let words = str.split(' ');
  return words.slice(1);
}

// whichCommand will parse the string and return the command being issued
function whichCommand(str) {
  if (str.indexOf('@') > -1){
    let words = str.split(' ');
    return words[0].split('@')[0];
  }
  let words = str.split(' ');
  return words[0];
}

/*************** Match Command to prexisiting one */
