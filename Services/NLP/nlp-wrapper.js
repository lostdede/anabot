/****

  Natural Language Processing Module

*****/

const { NlpManager } = require('node-nlp');

const manager = new NlpManager({ languages: ['en'] });
manager.load(`${__dirname}/model.nlp`);

(async() => {
    console.log(`[AnaBot.NLP] Ready to go.`)
})();

module.exports.AnaNLP = manager;
