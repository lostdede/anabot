let responseGroups = {
  user: {
    cannotsleep:[],
    testing:[],
    back:[],
    bored:[],
    busy:[],
    likeagent:[],
    lovesagent: [],
    needsadvice: [],
    excited: [],
    angry: []
  },
  agent: {
    acquaintance:[],
    age:[],
    annoying:[],
    bad:[],
    beautiful:[],
    beclever:[],
    birthday: [],
    boring: [],
    boss:[],
    busy:[],
    canyouhelp:[],
    chatbot:[],
    clever:[],
    crazy:[],
    fire: [],
    funny:[],
    good:[],
    happy:[],
    hobby:[],
    hungry:[],
    myfriend:[],
    marryuser: [],
    occupation:[],
    origin: [],
    real: [],
    ready: [],
    residence: [],
    right: [],
    sure: [],
    talktome: [],
    there: []
  },
  dialog: {
    holdon:[],
    hug:[],
    idontcare:[],
    sorry:[]
  },
  greetings: {
    howareyou: [],
    nicetomeetyou: [],
    nicetoseeyou:[],
    nicetotalktoyou:[]
  },
  appraisal: {
    good: [],
    noproblem: [],
    thankyou: [],
    welcome: [],
    welldone: []
  },
  misc: {}
}
