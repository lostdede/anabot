/*

  Guidelines

  Enter new data into the document.
  document.set({
    title: 'Welcome to Firestore',
    body: 'Hello World',
  }).then(() => {
    // Document created successfully.
  });

  Update an existing document.
  document.update({
    body: 'My first Firestore app',
  }).then(() => {
    // Document updated successfully.
  });

  Read the document.
  document.get().then(doc => {
    // Document read successfully.
  });

  Delete the document.
  document.delete().then(() => {
    // Document deleted successfully.
  });
*/
const F = require('../Config/fireconfig');
const TimeStamp = require('./logger').fieldTimeStamp;
const Log = require('./logger').Log;


class S3 {
	constructor(){
		this.addNewUser = addNewUser;
		this.addsuggestion = addsuggestion;
		this.adminGroup = ['lostdede'];
		this.Users = [];
    
		F.users.onSnapshot(snapshot => {
			snapshot.docChanges().forEach(change => this.Users.push(change.doc.data()));
		});
	}

	getUserChat(username){
		return this.Users.filter(x => x.username == username)[0];
	}
  
	addUserReminder(msg, newTask) {
    let currentReminders = this.Users.filter(x => x.username == msg.from.username || x.id == msg.chat.id)[0].reminders;
    if (currentReminders.length == 0) {
      currentReminders = [newTask];
    } else {
      currentReminders.push(newTask);
    }
		F.users.where('username', '==', msg.from.username).get().then(snapshot => snapshot.forEach(doc => F.users.doc(doc.id).update({
			reminders: currentReminders
		})));
	}

	clearUserReminders(msg) {
		F.users.where('username', '==', msg.from.username).get().then(snapshot => snapshot.forEach(doc => F.users.doc(doc.id).update({
			reminders: []
		})));
	}
}

module.exports.S3 = new S3();

// Add Chat will write a new user to a given document
function addNewUser(msg) {
	F.users.add({
		name: `${msg.from.first_name} ${msg.from.last_name}`,
		type: (msg.from.type == 'group' || msg.from.type == 'supergroup') ? 'group' : 'person',
		username: msg.from.username ? msg.from.username : msg.chat.title,
		id: msg.chat.id,
		reminders: []
	}).then(() => Log.newUser(msg.from.username));
}

function addsuggestion(user,msg){
	F.suggestions.add({
		user: user,
		completed: false,
		task: msg,
		time: TimeStamp()
	}).then(() => Log.suggestion(user,msg));
}
