const exec = require('child_process').exec;
const logs = require('../logger').Log;

module.exports.sysCommands = {
  runApiTests:runApiTests,
  updateRepo:updateRepo
};


function runApiTests(){
  exec('npm run test-api',function(err, stdout, stderr) {
    if (err) throw err;
    else console.log(stdout);
});
}

// Remember to add safe guard for pushes. WE REALLY REALLY NEED IT.
function updateRepo(){
  logs.codeUpdate('Process starting, please try in a bit to see if she has crashed.');
  exec('git pull && echo Updated',function(err, stdout, stderr) {
    if (err) {
      logs.codeUpdate(err);
      throw err
    }
    else {
      logs.codeUpdate(stdout);
      console.log(stdout);
    }
});
}
