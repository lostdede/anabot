const S3 = require('./s3').S3;
const Logs = require('./logger').Log;

const telegram = require('./bothandler').botHandler;

const Commands = {
	public:{
		entry: ['/start','/hello','/begin'],
		suggestions: '/suggest', // Take Suggestions from peasants
		reminders: '/showreminders', // Show user reminders
		addReminder: '/newreminder',
		clearReminders: '/deletereminders'
	},
	private: {
		messagePerson: '/sendTo', // Send message to specific person
		messageGroup: '/sendToGroup', // Send message to group
		messageDiagnostics: '/showConsole', // Show if any issues are in logger
		logs: '/showlogs', // Send bot logs
		messageMasses: '/broadcast', // Send message to all users
		whoarethey: '/whowegot', // Return list of all collected users
		getChatInfo: '/getChatInfo',
		showCommands: '/showCommands',
		apiTest: '/APITest',
		addExec: '/addToExec',
		removeExec: '/removeExec',
		viewExecs: '/execs'
	}
};

function isPublicCommand(incoming) {
	let publicCommands = [].concat.apply([], Object.values(Commands.public));
	if (publicCommands.includes(incoming)) {
		return true;
	}
	else {
		return false;
	}
}

function handleCommand(command,msg){
	switch (command) {
	case Commands.private.messagePerson:
		sendPersonMessage(msg);
		break;
	case Commands.private.messageGroup:
		sendGroupMessage(msg);
		break;
	case Commands.private.messageDiagnostics:

		break;
	case Commands.private.messageMasses:
		msg.reply.text(`Sending your message to  ${S3.Users.length} users.`);
		sendAllMessages(msg);
		break;
	case Commands.private.whoarethey:
		sendAllUsers(msg);
		break;
	case Commands.private.logs:
		sendLogs(msg);
		break;
	case Commands.private.getChatInfo:
		sendChatInfo(msg);
		break;
	case Commands.private.showCommands:
		sendCommands(msg);
		break;
	case Commands.private.apiTest:
		testAPI(msg);
		break;
	case Commands.private.addExec:
		addExec(msg);
		break;
	case Commands.private.removeExec:
		removeExec(msg);
		break;
	case Commands.private.viewExecs:
		sendExecs(msg);
		break;
	case Commands.public.entry:
		startFlow(msg);
		break;
	case Commands.public.suggestions:
		recordSuggestion(msg);
		break;
	case Commands.public.reminders:
		showUserReminders(msg);
		break;
	case Commands.public.addReminder:
		newReminder(msg);
		break;
	case Commands.public.clearReminders:
		clearUserReminders(msg);
	default:
	}
}

module.exports.CommandList = {
	Commands: Commands,
	handleCommand:handleCommand,
	isPublic: isPublicCommand
};

/*********************************

  SECTION: Public Command Helpers

**********************************/
function startFlow(msg){
	let username = msg.from.username;
	if (!(username in S3.Users)){
		Logs.newUser(username);
		S3.addNewUser(msg);
	}
	return telegram.sendMessage(msg.from.id, `Hello, ${ msg.from.first_name }!`);
}

function recordSuggestion(msg){
	let messagebody = msg.text.split(' ').slice(1).join(' ');
	let user = msg.from.username ? msg.from.username : msg.from.id;
	S3.addsuggestion(user,messagebody);
	return telegram.sendReply(msg, `Thanks ${ msg.from.first_name }! Look forward to an update`);
}

function sendChatInfo(msg){
	return telegram.sendPrivateMessage(msg,`Requested chat info: ${JSON.stringify(msg.chat)}`);
}

/*********************************

  SECTION: Private Command Helpers

**********************************/
function sendPersonMessage(msg){
	let processedMsg = msg.text.split(' ');
	let reciever = processedMsg[1];
	let messagebody = processedMsg.slice(2).join(' ');
	msg.reply.text(`Sent '${messagebody}' to ${reciever}`);
	return telegram.sendMarkdownMessage(S3.getUserChat(reciever),messagebody);
}

function sendGroupMessage(msg){
	let processedMsg = msg.text.split('-');
	let reciever = processedMsg[0].split(' ').slice(1).join('');
	let messagebody = processedMsg.slice(1).join(' ');
	msg.reply.text(`Sent '${messagebody}' to ${reciever}`);
	return telegram.sendMarkdownMessage(S3.getUserChat(reciever),messagebody);
}

// Broadcast to all users
function sendAllMessages(msg){
	let messageBody = msg.text.split(' ').slice(1).join(' ');
	for (let property in S3.Users) {
		telegram.sendMarkdownMessage(property.id,messageBody);
	}
}

// Send all users
function sendAllUsers(msg) {
	telegram.sendReply(msg, `*These are the currently avaliable users:*\n--------------------\n -${ S3.Users.map(x => x.username).join('\n -') }`);
}

// Send logs to user
function sendLogs(msg){
	telegram.sendReply(msg, `*BOT Logs*\n--------------------\n -${Logs.botRecords.join('\n -')}`);
	telegram.sendReply(msg, `*API Logs*\n--------------------\n ${Logs.apiRecords.join('\n')}`);
}

// Send list of commands
function sendCommands(msg){
	let allCommands = [];

	for (var i = 0; i < Object.keys(Commands.public).length; i++) {
		allCommands.push(` - ${Object.keys(Commands.public)[i]}: ${Commands.public[Object.keys(Commands.public)[i]]}\n`);
	}

	for (var i = 0; i < Object.keys(Commands.private).length; i++) {
		allCommands.push(` - ${Object.keys(Commands.private)[i]}: ${Commands.private[Object.keys(Commands.private)[i]]}\n`);
	}

	telegram.sendReply(msg,`*List of Commands*\n--------------------\n${allCommands.join('')}`);
}

// Test our api routes
function testAPI(msg){
	telegram.sendPrivateMessage(msg,`Starting *API Test*...\n...but it will never finish. Use _Postman_`);
}

// Update with latest version from repo
function updateCode(msg){
	telegram.sendPrivateMessage(msg,`You've just asked to _*update*_ the codebase.`);
}

function addExec(msg) {
	let messagebody = msg.text.split(' ')[1];
	if (S3.adminGroup.includes(messagebody)) {
		telegram.sendReply(msg,`*${messagebody}* is already part of the execs.`);
	}
	else {
		S3.adminGroup.push(messagebody);
		telegram.sendReply(msg,`Okay, I'll add *${messagebody}* to the fam.`);
	}
}

function removeExec(msg) {
	let messagebody = msg.text.split(' ')[1];
	if (messagebody === 'lostdede') {
		telegram.sendReply(msg,`I'm not going to remove that person, sorry ${msg.from.first_name}`);
	}
	var index = S3.adminGroup.indexOf(messagebody);
	if (index > -1) {
		S3.adminGroup.splice(index, 1);
		telegram.sendReply(msg,`Okay, I'll revoke *${messagebody}*.`);
	}
}

function sendExecs(msg){
	telegram.sendReply(msg,`*Super Users:*\n--------------------\n -${S3.adminGroup.join('\n - ')}`)
}


function showUserReminders(msg) {
	let remindersForID = S3.Users.filter(x => x.username == msg.from.username || x.id == msg.chat.id)[0].reminders;
	if (remindersForID.length == 0) {
		remindersForID = 'You have no reminders set.';
	} else {
		remindersForID = remindersForID.join('\n - ');
	}
	telegram.sendMarkdownMessage(msg.chat.id,`Okay ${msg.from.first_name}, here are your reminders:\n - ${remindersForID}`).catch(function(error) {console.log(error);});
}

function newReminder(msg){
	let newTask = msg.text.replace('/newreminder ','');
	if (newTask == '') {
		telegram.sendMarkdownMessage(msg.chat.id, 'Sorry, please type your reminder after the /newreminder command.\n _/newreminder <reminder>_');
		return;
	}
	S3.addUserReminder(msg, newTask);
	telegram.sendMarkdownMessage(msg.chat.id, 'Alrighty! I will remember that for you. Use the _`/reminders`_ command to view your reminders.')
}

function clearUserReminders(msg){
	S3.clearUserReminders(msg);
}