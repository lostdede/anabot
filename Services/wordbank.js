/*********************

  Responses for Bots

**********************/

/*

	Word Bank Module - Extended
	Started November 10th, 2017
  Edede Oiwoh

*/

module.exports.reqPhrase = function (preq){
  return Phrases[preq][Math.floor(Math.random() * Phrases[preq].length)];
}


// wordreq(word_request) returns a random word similar in meaning to the requested category.
// Example: If I want a word similar to yes:
//								wordreq('yes') -> 'yeah'||'okay'||'yup'
module.exports.reqWord = function (word_request){
								var ind = Math.floor(Math.random() * Words[word_request].length);
								return Words[word_request][ind].word;
							}

// contains(str,cat) returns a bool when it determines if str is in the word list of
// cat.
// Requirements: Str Str ->. Bool
module.exports.contains = function(str,cat){
								var category = eval(cat);
								for (var i = 0; i <= category.length - 1; i++) {
									if (str.toLowerCase() === category[i].word){
										return true;
									}
								}
								return false;
							}

// Phrases contains lists of phrases that match specific intentions and categories
let Phrases = {
  dimissStrangers: ["I don't know who you are, please go away..", "I don't feel comfortable talking to you.","I don't know who you are or what you want", "Honestly, you're just not verified so I can't really talk to you.", "You're not on my 'ok' list bud.", "I know you want to talk to me but maybe some other time okay?","Please.......just.......leave....", "Wow, hahahaha, I gotta go."]
}

// Words contain lists of words that match specific intentions and categories
let Words = {
  hi:[
  	{"word":"hi","score":94446,"tags":["syn","n"]},
  	{"word":"howdy","score":93563,"tags":["syn","n"]},
  	{"word":"how do you do?","score":72882,"tags":["syn","n"]},
  	{"word":"greetings","score":70706,"tags":["n"]},
  	{"word":"bonjour","score":69426,"tags":["n"]},
  	{"word":"hola","score":68382,"tags":["n"]},
  	{"word":"hey","score":66306,"tags":["n","prop"]},
  	{"word":"hallo","score":65804,"tags":["n"]},
  	{"word":"salutations","score":65182,"tags":["n"]},
  	{"word":"hiya","score":64976,"tags":["n"]},
  	{"word":"holla","score":64430,"tags":["n"]}],
  yes:[
    	{"word":"yeah","score":20572,"tags":["adv"]},
    	{"word":"yea","score":19360,"tags":["n"]},
    	{"word":"okay","score":16741,"tags":["adj"]},
    	{"word":"yay","score":16668,"tags":["n"]},
    	{"word":"yep","score":16586},
    	{"word":"aye","score":16473,"tags":["n"]},
    	{"word":"yeses","score":16439,"tags":["n"]},
    	{"word":"yup","score":15969},
    	{"word":"indeed","score":15535,"tags":["adv"]},
    	{"word":"alright","score":14776,"tags":["adv"]},
    	{"word":"affirmative","score":14701,"tags":["adj","n"]}],
  maybe:[
  	{"word":"maybe","score":16351,"tags":["adv"]}],
  ok:[
  	{"word":"alright","score":56212,"tags":["syn","adv"]},
  	{"word":"ok","score":55547,"tags":["syn","adj"]},
  	{"word":"fine","score":40440,"tags":["syn","adj"]},
  	{"word":"okey","score":40283,"tags":["syn","n","prop"]},
  	{"word":"satisfactory","score":37509,"tags":["syn","adj"]},
  	{"word":"all right","score":34123,"tags":["syn","adj","adv"]},
  	{"word":"approve","score":33474,"tags":["syn","v"]},
  	{"word":"okeh","score":29072,"tags":["syn","n","prop"]},
  	{"word":"all-right","score":28107,"tags":["syn"]}
  ],
  no:[
  	{"word":"nary","score":61161,"tags":["syn","adj","v","n"]},
  	{"word":"zero","score":55399,"tags":["syn","n","v","adj"]},
  	{"word":"no more","score":51677,"tags":["syn","adv"]},
  	{"word":"negative","score":39507,"tags":["adj"]},
  	{"word":"never","score":39507,"tags":["adv"]},
  	{"word":"none","score":39507,"tags":["n"]},
  	{"word":"not","score":39507,"tags":["adv"]},
  	{"word":"stop","score":39507,"tags":["v","n"]},
  ],
  agreed:[
  	{"word":"okay","score":48629,"tags":["syn","adj"]},
  	{"word":"alright","score":48229,"tags":["syn","adv"]},
  	{"word":"cool","score":48229,"tags":["syn","adj","v"]},
  	{"word":"great","score":48229,"tags":["syn","adj"]},
  	{"word":"swell","score":48229,"tags":["syn","n","v"]},
  	{"word":"wonderful","score":48229,"tags":["syn","adj"]}],
  anything:[
  	{"word":"anything","score":90000,"tags":["n"]},
  	{"word":"something","score":75731,"tags":["n"]},
  	{"word":"random info","score":65214,"tags":["adv"]},
  	{"word":"surprise me","score":65214,"tags":["adv"]},
  	{"word":"just anything","score":65214,"tags":["adv"]}
  ],
  everything:[
  	{"word":"everything","score":90214,"tags":["adv"]},
  	{"word":"entire thing","score":63294,"tags":["adv"]},
  	{"word":"entire","score":62817,"tags":["adj"]},
  	{"word":"total","score":54147,"tags":["n"]},
  	{"word":"complete section","score":65214,"tags":["adv"]},
  	{"word":"complete document","score":65214,"tags":["adv"]},
  	{"word":"whole thing","score":65214,"tags":["adv"]},
  	{"word":"all of it","score":65214,"tags":["adv"]}
  ],
  bye:[
  	{"word":"adios","score":99084,"tags":["syn","n"]},
  	{"word":"au revoir","score":97630,"tags":["syn","n"]},
  	{"word":"adieu","score":96996,"tags":["syn","n"]},
  	{"word":"arrivederci","score":95232,"tags":["syn","n"]},
  	{"word":"sayonara","score":95091,"tags":["syn","n"]},
  	{"word":"cheerio","score":94586,"tags":["syn","n"]},
  	{"word":"goodbye","score":94581,"tags":["syn","n"]},
  	{"word":"auf wiedersehen","score":93500,"tags":["syn","n"]},
  	{"word":"bye","score":89373,"tags":["syn","n"]},
  	{"word":"so long","score":84662,"tags":["syn","n"]},
  	{"word":"good day","score":80705,"tags":["syn","n"]},
  	{"word":"bye-bye","score":77589,"tags":["syn","n"]},
  	{"word":"good-bye","score":77589,"tags":["syn","n"]},
  	{"word":"farewell","score":75740,"tags":["n"]}
  	]
}
