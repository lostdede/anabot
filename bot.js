/**************************

  Telegram Bot

***************************/
require('dotenv').config();
const bot = require('./Config/setup').bot;
const C = require('./Services/commands').CommandList;
const S3 = require('./Services/s3').S3;
const P = require('./Services/textParse').Parser;
const Logs = require('./Services/logger').Log;
const lp = require('./Services/NLP/nlp-wrapper').AnaNLP;

bot.start();

// On any incoming message handle it
bot.on('text', (msg) => {
	let incoming = msg.text;
	let username = msg.from.username ? msg.from.username : msg.from.id;
	if (S3.adminGroup.includes(username) || S3.adminGroup.includes(msg.from.id)){
		if (P.isCommand(incoming)){
			let command = P.whichCommand(incoming);
			Logs.ranCommand(username,command);
			C.handleCommand(command,msg);
		}
		else {
			Logs.personMessaged(username,msg.text);
			(async() => {
				const response = await lp.process('en', msg.text);
				bot.sendChatAction(msg.chat.id,'typing');
				msg.reply.text(response.answer);
			})();
		}
	}
	else {
		if (P.isCommand(incoming)){
			let command = P.whichCommand(incoming);
			if (C.isPublic(command)){
				C.handleCommand(command,msg);
				Logs.ranCommand(username,command);
			}
			else {
				Logs.triedCommand(username,P.whichCommand(incoming));
				msg.reply.text(`Sorry ${ msg.from.first_name }, you can't run this command.`);
			}
		}
		else {
			Logs.personMessaged(username,msg.text);
			(async() => {
				const response = await lp.process('en', msg.text);
				bot.sendChatAction(msg.chat.id,'typing');
				msg.reply.text(response.answer);
			})();
		}
	}
});

// When a user edits a message
bot.on('edit', (msg) => {
	return msg.reply.text(`Hahaha I saw that! You edited your message ${ msg.from.first_name }!`, { asReply: true });
});

// On a bunch of things I haven't coded yet, just say thank you and log it
bot.on(['location', 'contact','audio','video','voice','photo'], (msg, self) => {
	Logs.personMessaged(msg.from.first_name,self.type);
	return bot.sendMessage(msg.from.id, `Thank you for the ${ self.type }.`);
});
