
# Ana Telegram Bot
---
The Ana project has been an ongoing crack at the challenges associated with creating a real world version of Tony Stark's J.A.R.V.I.S program. This is the latest iteration of interfaces using the Telegram Bot API as an entry point.


## Installation and Running

### Server
- Install Dependencies `npm install`
- Start application `npm watch`

#### Local Machine
- Install dependencies with `npm install`
- Start application `node bot.js`

## Branches
There are two significant branches:
- Master
- Prod

Master is the most up to date, and all development branches come off master and merge back into master. Prod is kept intact for deployment to a server. *Only admins can merge master into prod*.

*Prod must always have the console logging feature disabled.* 

## Users
There are two general user types for Ana:
- Admins
- Public

Public members aren't trusted and as such have limited access to Ana's capabilities. Most of Ana's functions are only available to members of the Admin Group. For now public members are dismissed whenever they try to run special commands.

## Functionality
These include features and upcoming additions to the project:
- Bitbucket Repository Updates
- Google Calendar Updates
- IFTTT Triggers
